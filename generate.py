import random
import argparse

def iterate_instructions(data, instructions, row, col):
    word = data[row][col]
    for end_row, end_col in instructions:
        word += data[end_row][end_col]
    return word

def generate_grid(size, dictionary_size):
    data = []
    for row in range(size[0]):
        row = []
        for col in range(size[1]):
            row.append(chr(ord('A')+random.randint(0, 25)))
        data.append(row)
        
    max_size = random.randint(0, max(size[0], size[1]) - 2) + 2
    dictionary = []
    for row in range(size[0]):
        for col in range(size[1]):
            down = range(row + 1, min(row + max_size, size[0]))
            up = range(max(0, row - (max_size - 1)), row)[::-1]
            right = range(col + 1, min(col + max_size, size[1]))
            left = range(max(0, col - (max_size - 1)), col)[::-1]
            if min(len(down), len(left)) > 1:
                dictionary.append(iterate_instructions(data, zip(down, left), row, col))
            if min(len(down), len(right)) > 1:
                dictionary.append(iterate_instructions(data, zip(down, right), row, col))
            if min(len(up), len(left)) > 1:
                dictionary.append(iterate_instructions(data, zip(up, left), row, col))
            if min(len(up), len(right)) > 1:
                dictionary.append(iterate_instructions(data, zip(up, right), row, col))
            if len(left) > 1:
                dictionary.append(iterate_instructions(data, zip([row] * len(left), left), row, col))
            if len(down) > 1:
                dictionary.append(iterate_instructions(data, zip(down, [col] * len(down)), row, col))
            if len(up) > 1:
                dictionary.append(iterate_instructions(data, zip(up, [col] * len(up)), row, col))
            if len(right) > 1:
                dictionary.append(iterate_instructions(data, zip([row] * len(right), right), row, col))
            
    random.shuffle(dictionary)
    return data, dictionary[0:dictionary_size]

def main():
    parser = argparse.ArgumentParser(description='Generate a puzzle for the alphabet soup')
    parser.add_argument('rows', type=int, help='number of rows for the puzzle')
    parser.add_argument('columns', type=int, help='number of columns per row for the puzzle')
    parser.add_argument('dictionary_size', type=int, help='number of items in the dictionary')

    args = parser.parse_args()
    data, dictionary = generate_grid((args.rows, args.columns), args.dictionary_size)
    print(str(args.rows) + "x" + str(args.columns))
    for row in data:
        print(" ".join(row))
    for word in dictionary:
        print(word)

if __name__ == "__main__":
    main()