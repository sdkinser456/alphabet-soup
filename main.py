import collections
import argparse
tree = lambda: collections.defaultdict(tree)

def iterate_instructions(data, dictionary, instructions, row, col):
    """
    Summary: a helper function to save out on some copy paste on Brute force, 
    essentially we want to traverse the grid in a certain direction
    we return a three tuple of strings.
    If we find the word before we are done going through the list we will leave.
    
    Parameters:
    data (two d array): the grid of the alphabet
    dictionary (set of strings): the list words we are looking for
    instructions (array of tuple ints): a zipped array of which x and y coordinates to go through, essentially where we want to go
    row (int): starting x coordinate
    col (int): starting y coordinate 

    Return:
    three tuple of the word and the starting and ending location
    Example 
    (Hello, 0:0, 5:5)
    """
    word = data[row][col]
    for end_row, end_col in instructions:
        word += data[end_row][end_col]
        if word in dictionary:
            return word, str(row)+ ":" + str(col), str(end_row) + ":" + str(end_col)
    return "", "", ""


def brute_force(size, data, dictionary):
    """
    Summary: 
    Essentially we want to navigate through the grid starting at each location where a key letter is
    then we want to navigate in the 8 directions to see if that starting letter has the word we care about
    and then we print out our distinct findings, so if the same word shows up multiple times, we only print the last occurance
    
    Parameters:
    size (tuple of ints): 
    data (two d array): the grid of the alphabet
    dictionary (set of strings): the list words we are looking for

    Return:
    output (dictionary of strings): a dictionary of the coordinates
    """
    output = {}
    # let's get the maximum lookahead so we know which words to search in the dictionary
    max_size = 0
    init_keys = set()
    for init_key in dictionary:
        init_keys.add(init_key[0])
        init_keys.add(init_key[-1])
        if len(init_key) > max_size:
            max_size = len(init_key)
    starting_coordinates = []
    for row in range(size[0]):
        for col in range(size[1]):
            if data[row][col] in init_keys:
                starting_coordinates.append((row, col))
    for (row, col) in starting_coordinates:
        down = range(row + 1, min(row + max_size, size[0]))
        up = range(max(0, row - (max_size - 1)), row)[::-1]
        left = range(col + 1, min(col + max_size, size[1]))
        right = range(max(0, col - (max_size - 1)), col)[::-1]
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip(up, [col] * len(up)), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip(down, [col] * len(down)), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip([row] * len(left), left), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip([row] * len(right), right), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip(up, right), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip(up, left), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip(down, right), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_instructions(data, dictionary, zip(down, left), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
    return output

def no_early_exit_iterate(data, dictionary, instructions, row, col):
    """
    Summary: a helper function to save out on some copy paste on Brute force, 
    essentially we want to traverse the grid in a certain direction
    we return a three tuple of strings.
    If we find the word we add it to the list
    
    Parameters:
    data (two d array): the grid of the alphabet
    dictionary (set of strings): the list words we are looking for
    instructions (array of tuple ints): a zipped array of which x and y coordinates to go through, essentially where we want to go
    row (int): starting x coordinate
    col (int): starting y coordinate 

    Return:
    a list of three tuples of the word and the starting and ending location
    Example 
    [(Hello, 0:0, 5:5), (He, 0:0, 2:2)]
    """
    words = []
    word = data[row][col]
    for end_row, end_col in instructions:
        word += data[end_row][end_col]
        if word in dictionary:
            words.append((word, str(row)+ ":" + str(col), str(end_row) + ":" + str(end_col)))
    return words

def brute_force_no_early_exit(size, data, dictionary):
    """
    Summary: 
    Essentially we want to navigate through the grid starting at each location where a key letter is
    then we want to navigate in the 8 directions to see if that starting letter has the word we care about
    and then we print out our distinct findings, so if the same word shows up multiple times, we only print the last occurance
    
    Parameters:
    size (tuple of ints): 
    data (two d array): the grid of the alphabet
    dictionary (set of strings): the list words we are looking for

    Return:
    output (dictionary of strings): a dictionary of the coordinates
    """
    output = {}
    # let's get the maximum lookahead so we know which words to search in the dictionary
    max_size = 0
    init_keys = set()
    for init_key in dictionary:
        init_keys.add(init_key[0])
        if len(init_key) > max_size:
            max_size = len(init_key)
    starting_coordinates = []
    for row in range(size[0]):
        for col in range(size[1]):
            if data[row][col] in init_keys:
                starting_coordinates.append((row, col))
    for (row, col) in starting_coordinates:
        down = range(row + 1, min(row + max_size, size[0]))
        up = range(max(0, row - (max_size - 1)), row)[::-1]
        left = range(col + 1, min(col + max_size, size[1]))
        right = range(max(0, col - (max_size - 1)), col)[::-1]
        words = no_early_exit_iterate(data, dictionary, zip(up, [col] * len(up)), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip(down, [col] * len(down)), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip([row] * len(left), left), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip([row] * len(right), right), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip(up, right), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip(up, left), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip(down, right), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
        words = no_early_exit_iterate(data, dictionary, zip(down, left), row, col)
        for word, coordinate_1, coordinate_2 in words:
            output[word] = coordinate_1 + " " + coordinate_2
    return output

def generate_tree(dictionary):
    """
    Summary: 
    a function to generate a B tree of characters, so we can look at all words at the same time when going through the alphabet soup

    Parameters:
    dictionary (set of strings): the list words we are looking for

    Return:
    root: a tree structure which is essentially a B tree
    """
    root = tree()
    for word in dictionary:
        path = [char for char in word]
        add_element(root, path, True)
        
    return root
def add_element(root, path, data):
    """
    Summary:
    a helper function to recursively add a value to a tree

    Parameters:
    root: the relative root of the tree
    path (array of strings): the path to value we care about
    data: the value we are trying to set to true

    Return:
    this doesn't return a value but it mutates the tree
    """
    if len(path) == 1:
        root[path[0]] = data
    else:
        add_element(root[path[0]], path[1:], data)

def iterate_while_traversing_tree(data, subtree, instructions, row, col):
    """
    Summary: a helper function to save out on some copy paste on Use Lookup tree function, 
    essentially we want to traverse the grid in a certain direction
    we return a three tuple of strings.
    If we find the word before we are done going through the list we will leave.
    instead of using the dictionary from above we use this tree to compare all possible words at the same time
    
    Parameters:
    data (two d array): the grid of the alphabet
    subtree (nested dictionary): a b tree that compares all possible words at the same time
    instructions (array of tuple ints): a zipped array of which x and y coordinates to go through, essentially where we want to go
    row (int): starting x coordinate
    col (int): starting y coordinate 

    Return:
    three tuple of the word and the starting and ending location
    Example 
    (Hello, 0:0, 5:5)
    """
    word = data[row][col]
    for end_row, end_col in instructions:
        word += data[end_row][end_col]
        if data[end_row][end_col] not in subtree:
            break
        subtree = subtree[data[end_row][end_col]]
        if subtree is True:
            return word, str(row)+ ":" + str(col), str(end_row) + ":" + str(end_col)
        
    return "", "", ""

def use_lookup_tree(size, data, dictionary):
    """
    Summary: 
    Essentially we want to navigate through the grid starting at each location where a key letter is
    then we want to navigate in the 8 directions to see if that starting letter has the word we care about
    and then we print out our distinct findings, so if the same word shows up multiple times, we only print the last occurance
    
    Parameters:
    size (tuple of ints): 
    data (two d array): the grid of the alphabet
    dictionary (set of strings): the list words we are looking for

    Return:
    output (dictionary of strings): a dictionary of the coordinates
    """
    root = generate_tree(dictionary)
    init_keys = set()
    max_size = 0
    for init_key in dictionary:
        init_keys.add(init_key[0])
        init_keys.add(init_key[:-1])
        if len(init_key) > max_size:
            max_size = len(init_key)
    starting_coordinates = []
    for row in range(size[0]):
        for col in range(size[1]):
            if data[row][col] in init_keys:
                starting_coordinates.append((row, col))
    output = {}

    for (row, col) in starting_coordinates:
        # look down columns until size
        subtree = root[data[row][col]]
        down = range(row + 1, min(row + max_size, size[0]))
        up = range(max(0, row - (max_size - 1)), row)[::-1]
        right = range(col + 1, min(col + max_size, size[1]))
        left = range(max(0, col - (max_size - 1)), col)[::-1]
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip(up, [col] * len(up)), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip(down, [col] * len(down)), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip([row] * len(left), left), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip([row] * len(right), right), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip(up, right), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip(up, left), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip(down, right), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2
        word, coordinate_1, coordinate_2 = iterate_while_traversing_tree(data, subtree, zip(down, left), row, col)
        if word != "":
            output[word] = coordinate_1 + " " + coordinate_2

    return output

def main():
    parser = argparse.ArgumentParser(description='Find the words from the alphabet soup')
    parser.add_argument('filename', type=str, help='filename to parse')
    parser.add_argument('--function-name', dest='function_name', default="brute_force",
                        choices=["brute_force", "use_lookup_tree", "brute_force_no_early_exit"],
                        help='which function to use when searching (default: brute_force)')

    args = parser.parse_args()

    size = (0,0)
    data = []
    dictionary = set()
    with open(args.filename) as f:
        first_line = f.readline().split("x")
        if len(first_line) != 2:
            raise Exception("Invalid file schema")
        size = (int(first_line[0]), int(first_line[1]))
        for x in range(size[0]):
            data.append(f.readline().strip().split(" "))
        for word in f.readlines():
            dictionary.add(word.strip())

    output = {}
    if args.function_name == 'brute_force':
        output = brute_force(size, data, dictionary)
    elif args.function_name == 'use_lookup_tree':
        output = use_lookup_tree(size, data, dictionary)
    elif args.function_name == 'brute_force_no_early_exit':
        output = brute_force_no_early_exit(size, data, dictionary)
    for key, value in output.items():
        print("{} {}".format(key, value))
    
if __name__ == "__main__":
    main()